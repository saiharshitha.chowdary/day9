class Solution {
    public int largestAltitude(int[] gain) {
         int curAltitude = 0;
        int highestAltitude = 0; // what we'll return
        
        for (int g : gain) {
            curAltitude += g;
            highestAltitude = Math.max(highestAltitude, curAltitude);
        }
        
        return highestAltitude;
    }
}
        