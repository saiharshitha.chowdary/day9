class Solution {
    public String reversePrefix(String word, char ch) {
          char[] c = word.toCharArray();
        int index = word.indexOf(ch) ;

        int j = index;
        for(int i = 0; i < (index+1)/2; i++) {
            char temp = c[i];
            c[i] = c[j];
            c[j--] = temp;
        }
        return String.valueOf(c);
    }
        
    }