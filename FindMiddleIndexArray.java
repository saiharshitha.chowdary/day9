class Solution {
    public int findMiddleIndex(int[] nums) {
         int sum = 0;
        for (final int el: nums) {
            sum += el;
        }
        
        int firstPart = 0, secondPart = sum;
        for (int i=0; i<nums.length; i++) {
            secondPart -= nums[i];
            if (firstPart == secondPart) {
                return i;
            }
            firstPart += nums[i];
        }
        return -1;
    }
}
        
    