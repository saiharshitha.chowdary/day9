import java.util.*;
class Solution {
    public int timeRequiredToBuy(int[] tickets, int k) {
        int time = 0;
        for (int i = 0; i <= k; i++) {
            time += Math.min(tickets[k], tickets[i]);
        }
        
        for (int i = k + 1; i < tickets.length; i++) {
            if (tickets[i] >= tickets[k]) {
                time += tickets[k] - 1;
            }
            
            else {
                time += tickets[i];
            }
        }
        return time;
    }
}
        
    