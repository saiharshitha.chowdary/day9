class Solution {
    public int maxAscendingSum(int[] nums) {
        int max = 0;
        int sum = 0;
        for(int i = 0;i<nums.length;i++){

            if(i+1>=nums.length){
                sum+=nums[i];
                max = Math.max(max,sum);
                break;
            }
            if(nums[i+1]>nums[i]){
                sum+=nums[i];
            }
            else{
                sum+=nums[i];
                max = Math.max(max,sum);
                sum = 0;
            }
        }
        return max;
    }
}
        
    