class Solution {
    public List<Integer> intersection(int[][] nums) {
         int n=nums.length;
        List<Integer>ans=new ArrayList<>();
       int f[]=new int[1001];
        for(int num[] : nums){
            for(int v : num) {
                if(++f[v]==n)ans.add(v);
            }
        }
        Collections.sort(ans);
        return ans; 
    }
}
        
    