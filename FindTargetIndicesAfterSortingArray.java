class Solution {
    public List<Integer> targetIndices(int[] nums, int target) {
        var targetCount = 0;
	var smallerCount = 0;

	for (var num : nums)
		if (num == target)
			targetCount++;
		else if (num < target)
			smallerCount++;

	var targets = new ArrayList<Integer>();
	for (var i = smallerCount; i < targetCount + smallerCount; i++)
		targets.add(i);
	return targets;
}
        
    }