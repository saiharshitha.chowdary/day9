class Solution {
    public int secondHighest(String s) {
            int greatestDigit = -1;
        int secondGreatestDigit = -1; // what we'll return
        
        for (char c : s.toCharArray()) {
            // '0'--'9' is 48--57 in ASCII
            if ((c <= 57)) { // if we have a digit...
                int curDigit = c - 48;
                if (curDigit > greatestDigit) {
                    secondGreatestDigit = greatestDigit;
                    greatestDigit = curDigit;
                } else if (curDigit == greatestDigit || curDigit == secondGreatestDigit) {
                    // do nothing if we've already categorized the current digit as the greatest or second greatest digit
                } else if (curDigit > secondGreatestDigit) {
                    secondGreatestDigit = curDigit;
                }
            }
        }
        
        return secondGreatestDigit;
    }
}
        
    