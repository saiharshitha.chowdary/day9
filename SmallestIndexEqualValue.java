class Solution {
    public int smallestEqual(int[] nums) {
        int i, n = nums.length;
        
        for(i = 0; i < n; i++){
            if(nums[i] == (i % 10)){
                return i;
            }
        }
        
        return -1;
    }
}
        
    